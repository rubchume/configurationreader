from dataclasses import dataclass


class ConfigurationTemplate:
    def __init__(self):
        self.variables = []

    @property
    def names(self):
        return [variable.name for variable in self.variables]

    def add(self, variable, post_process=None):
        self.variables.append(ConfigurationVariableTemplate(variable, post_process))

    def get_variable_by_name(self, name):
        return next(filter(lambda variable: variable.name == name, self.variables))


@dataclass
class ConfigurationVariableTemplate:
    name: str
    post_process: callable = None
