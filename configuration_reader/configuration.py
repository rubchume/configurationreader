import argparse
import json
import os
import sys
from pathlib import Path

from .configuration_template import ConfigurationTemplate


CONFIGURATION_FILE = "configuration.json"


configuration = None


def read_configuration_file():
    if Path(CONFIGURATION_FILE).is_file():
        with Path(CONFIGURATION_FILE).open("r") as conf_file:
            return json.load(conf_file)


# def parse_command_line_arguments():
def create_argument_parser(configuration_template):
    parser = argparse.ArgumentParser()
    for name in configuration_template.names:
        parser.add_argument(f"--{name}", type=str, nargs="?")
    return parser


def parse_command_line_arguments(configuration_template):
    parser = create_argument_parser(configuration_template)
    parsed_arguments = parser.parse_args(sys.argv[1:])

    return {
        key: value
        for key, value in vars(parsed_arguments).items()
        if value is not None
    }


def filter_configuration_variables(configuration, configuration_template):
    return {
        key: value
        for key, value in configuration.items()
        if key in configuration_template.names
    }


def load_configuration(configuration_template: ConfigurationTemplate):
    global configuration

    file_configuration = read_configuration_file()
    environment_variables = dict(os.environ)
    command_line_arguments = parse_command_line_arguments(configuration_template)
    configuration = filter_configuration_variables(dict(
        file_configuration,
        **environment_variables,
        **command_line_arguments
    ), configuration_template)

    for variable_name, value in configuration.items():
        variable = configuration_template.get_variable_by_name(variable_name)
        if variable.post_process is not None:
            configuration[variable_name] = variable.post_process(variable_name, value)


def get_configuration():
    return configuration
