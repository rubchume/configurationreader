Put your configuration in JSON format a file called `configuration.json`:

```JSON
{
  "myvariable1": "myvalue1",
  "another_variable": 123,
  "third_variable": "hola"
}
```

Another example
```JSON
{
  "add_to_cart": "http://127.0.0.1:4444/wd/hub",
  "COOKIES": {"mid": "YmEOjwAEAAECeEN634NMZj3e29T6", "ig_did": "C6B222B6-CC04-4CC6-A2B8-77154D628443", "fbm_124024574287414": "base_domain=.instagram.com", "csrftoken": "XOg6zKGQ58rqfZ37SabtRC7vj5m9v2rz", "ds_user_id": "9687473625", "sessionid": "9687473625:dBETFrYUCodJ38:16", "shbid": "\"17457,9687473625,1682100499:01f71ffe2ec2f43ed47834836f1b157dc067f4996ede319a7a8f62e3600c46ea278d0346\"", "shbts": "\"1650564499,9687473625,1682100499:01f77f936cc9ceb561a00c3d4cd82ac0bdd86ab72d10fb692a6f8cbf46d5f1b4925ead60\"", "fbsr_124024574287414": "fnDVlwkBS9ZOTFMSJfe4OMnI5Eubpyik5liX5rofqIQ.eyJ1c2VyX2lkIjoiMTEyODg4NTg1MyIsImNvZGUiOiJBUUNndDNZaVZFdU1ZMmVVTzFZVmRZc1FkY3U0ckhvZzEybFcxLWloVnNQVWNNUGhaWUZLTlQ4Z3dpOWZUYVJVaWZIVl9RcVB1aXk2bGJsYlg2MWZLdko0R1E1V3h1SjRvYkhBX2Y0ZTAxRkoyMnM5dGZJNFJHc0xabUswbGxUa3g3am1pR2FCMGhKYU9jY2FLQmhjRGoxVnZHM0VjS1YyVThLaWRqR3djd1RSVWdvMGd1Wlo2dHBJVzg1TEctN1NEYklVN1JJRW9TOEkwVDhmS21GVWFlX1lSaW5KMXVWOWFyNDFhUDdPNlFWaGpjVGhSSmtpSTlnMmdSMkw2ZmtNV0h4alBkU1ZYRXNQbmg0QVRyUk5CV0RLM2tqLXI2T3J0YnEwSE0yeWV5enlwYzVsc3NGLUVIc1pqRzdOSjlUdjEtVSIsIm9hdXRoX3Rva2VuIjoiRUFBQnd6TGl4bmpZQkFKWkJqRzZ4R3VNUmUySnEwVG1RUVBJbW5aQWIzc1VkRGRWMEZ5V3hHMkdVeXdXb3RKczlaQlVPRW1LbU9GYlZiN1pBSzVIb0k0ZlY3VFJaQVJjdnVSS0pzZkJWdHROdGpEaEdaQzd2a2hJZk9uSEZaQUdOUGZWZzhRT3dqTjNaQ0Z1SHBWUFV0ZFdnVk1hZ3EybVpBUG84TVpBZjhta1IycVE2Q1JaQlNOcjVDbTUiLCJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImlzc3VlZF9hdCI6MTY1MDU2ODMwNH0", "rur": "\"ODN,9687473625,1682104539:01f7797796c325f1451cab77aa5ce3287a6d7f10f3f84ceb14621892cd7e9032d257513c\""},
  "AWS_ACCESS_KEY_ID": "AKIA54XRJLQWQ2CVC4KR",
  "AWS_SECRET_ACCESS_KEY": "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"
}
```

Then, in `configuration_template_customization.py` you add the configuration variables that are allowed. 
You can specify post processing functions, or just leave them empty.
```python
import json
import os

from .configuration_template import configuration_template


configuration_template.add("SELENIUM_URL")


def example_post_processing(variable_name, value):
    if isinstance(value, str):
        return json.loads(value)
    else:
        return value


configuration_template.add("myvariable1", example_post_processing)


def set_environment_variable(variable_name, value):
    os.environ[variable_name] = value
    return value


configuration_template.add("another_variable", set_environment_variable)

configuration_template.add("third_variable")
```

Finally, just with one import you have available a dictionary where keys are variable names and values are their configuration values.
```python
from configuration.configuration import configuration
```
